# This is a test daemon program

import socket
import sys
import threading
import requests
import ups_pb2
import amazon_pb2
import amazonUPS_pb2
from google.protobuf.internal.encoder import _VarintBytes
from google.protobuf.internal.decoder import _DecodeVarint32

'''
worldHost = "vcm-903.vm.duke.edu"
worldPort = 12345
amazonPort = 23333
amazonHost = "vcm-2743.vm.duke.edu"
amazonListenPort = 9999
#worldID = 1001
numTrucksInit = 5
#bufferLen = 1024
simSpeed = 999999999
localHost = "vcm-903.vm.duke.edu"
'''

worldHost = sys.argv[1]
worldPort = int(sys.argv[2])
amazonPort = int(sys.argv[3])
amazonHost = sys.argv[4]
amazonListenPort = int(sys.argv[5])
numTrucksInit = int(sys.argv[6])
simSpeed = int(sys.argv[7])
localHost = sys.argv[8]


# These are the website we are using
aRequestTruck = "http://"+localHost+":8000/ups/aRequestTruck/"
aLoaded = "http://"+localHost+":8000/ups/aLoaded/"
aRequestPackID = "http://"+localHost+":8000/ups/aRequestPackID/"
aAddProduct = "http://"+localHost+":8000/ups/aAddProduct/"
aTruckToGo = "http://"+localHost+":8000/ups/aTruckToGo/"
aGetPackDes = "http://"+localHost+":8000/ups/aGetPackDes/"
wTruckCompletion = "http://"+localHost+":8000/ups/wTruckCompletion/"
wPackageDelivered = "http://"+localHost+":8000/ups/wPackageDelivered/"

def sendMsg(sock, msg):
    msgBytes = msg.SerializeToString()
    
    buf = b''
    buf = _VarintBytes(msg.ByteSize())
    buf += msgBytes
    
    #sock.send(_VarintBytes(msg.ByteSize()))
    sock.send(buf)

'''
def recvMsg(sock):
    data = b''
    flag = 0 # This flag indicates if it should receive size info or data

    while True:
        if flag == 0:
            size = sock.recv(1)
            print(size)
            flag = 1
            msg_len, msg_pos = _DecodeVarint32(size, 0)
        else:
            data += sock.recv(msg_len)
            if (len(data) == msg_len):
                print(data)
                break
    return data
'''

def decode_varint(iterable):
    shift = 0
    n = 0
    for byte in iterable:
        n |= (byte & 0x7f) << shift
        shift += 7
        if byte & 0x80 == 0:
            break
    else: # didn't see a terminating byte
        raise EOFError
    return n

def socket_byte_iterator(reader):
    while True:
        byte = reader.recv(1)
        if not byte:
            break
        yield ord(byte)

def recvMsg(sock):
    length = decode_varint(socket_byte_iterator(sock))
    recv_bytes = sock.recv(length)
    #obj.ParseFromString(recv_bytes)
    return recv_bytes

        
# This is a thread class to handle all the amazon commands
class AmazonCmd(threading.Thread):
    def __init__(self, clientSocket, toWorldSocket):
        threading.Thread.__init__(self)
        self.clientSocket = clientSocket
        self.toWorldSocket = toWorldSocket

    def run(self):
        # Since this is a listening port, received msg must be Acommands
        amazonCmd = amazonUPS_pb2.Acommands()
        amazonInfo = recvMsg(self.clientSocket)
        amazonCmd.ParseFromString(amazonInfo)
        print(amazonCmd)

        # Determine what type of command it is
        if amazonCmd.HasField("request_package_id"):
            # Amazon wants a new package id
            warehouseID = amazonCmd.request_package_id.wh_id
            destination_x = amazonCmd.request_package_id.x
            destination_y = amazonCmd.request_package_id.y
            # user is an optional field
            user = -1
            if (amazonCmd.request_package_id.HasField("ups_user_id")):
                user = amazonCmd.request_package_id.ups_user_id

            # Send the information all the information to the string django view using JSON file
            packDetail = {}
            packDetail["user"] = user
            packDetail["x"] = destination_x
            packDetail["y"] = destination_y
            packDetail["whID"] = warehouseID
            newPackIDReturn = requests.post(aRequestPackID, json=packDetail)
            newPackIDJson = newPackIDReturn.json()
            newPackageID = newPackIDJson["newID"]

            # Send the information about each item back to the database
            for product in amazonCmd.request_package_id.items:
                productDetail = {}
                productDetail["pack_id"] = newPackageID
                productDetail["item_id"] = product.item_id
                productDetail["description"] = product.description
                productDetail["amount"] = product.amount
                updateProduct = requests.post(aAddProduct, json=productDetail)
                updateProductDetail = updateProduct.json()
                if updateProductDetail["status"] == 0:
                    print("New product has been added to the package", newPackageID)
                else:
                    print("New product has been failed to be added to the package", newPackageID)
                    
            # Send the return packageid back to amazon
            packIDResponse = amazonUPS_pb2.Uresponses()
            packIDResponse.response_package_id.package_id = newPackageID
            if newPackageID == -1:
                # There is an error, probably the user typed the wrong ID
                packIDResponse.response_package_id.error = "There is an error. Probably the user ID is incorrect"

            # Send the response back to Amazon
            sendMsg(self.clientSocket, packIDResponse)
            self.clientSocket.close()
        elif amazonCmd.HasField("request_truck"):
            # Amazon wants a truck to a warehouse
            destinationWH = amazonCmd.request_truck.wh_id
            # Send the ack back to Amazon
            pickUpAck = amazonUPS_pb2.Uresponses()
            pickUpAck.acknowledge.success = True
            sendMsg(self.clientSocket, pickUpAck)
            self.clientSocket.close()
            
            # Send the information back to database
            # If there's a truck available, send to the world so the truck can pickup
            # If there's no truck available, it is in the databse
            task = {}
            task["wh_id"] = destinationWH
            hasFreeTruck = requests.post(aRequestTruck, json=task)
            freeTruck = hasFreeTruck.json()
            
            # If there is no free truck, we don't do anything at this point
            if freeTruck["truck_id"] != -1:
                # There is a free truck. Send it to warehouse
                sendTruckWH = ups_pb2.UCommands()
                sendTruckWH.simspeed = simSpeed
                pickUpIns = sendTruckWH.pickups.add()
                pickUpIns.truckid = freeTruck["truck_id"]
                pickUpIns.whid = destinationWH
                sendMsg(self.toWorldSocket, sendTruckWH)
        elif amazonCmd.HasField("packages_loaded"):
            # Amazon informs us all packages have been loaded
            loadedTruck = amazonCmd.packages_loaded.truck_id
            loadedWH = amazonCmd.packages_loaded.wh_id
            
            # Send back the acknowledgement
            loadAck = amazonUPS_pb2.Uresponses()
            loadAck.acknowledge.success = True
            sendMsg(self.clientSocket, loadAck)
            self.clientSocket.close()
            
            # Need to import all the info about package_id, and update them to be loaded
            # Send this info back to the database
            for pack in amazonCmd.packages_loaded.package_id:
                onBoard = {}
                onBoard["packID"] = pack
                sig = requests.post(aLoaded, json=onBoard)
                sigJson = sig.json()
                if sigJson["status"] == -1:
                    print("There is an error while registering for loaded package")

            # Tell the database to change the truck's status to delivering
            truckToGo = {}
            truckToGo["truckID"] = loadedTruck
            truckToGo["whID"] = loadedWH
            truckRes = requests.post(aTruckToGo, json=truckToGo)
            truckResJson = truckRes.json()
            
            # Result shows if it is a success
            if truckResJson["result"] == 0:
                print("Truck ", loadedTruck, " is set to be in delivery")
                # Form the truck sending message
                toDelivery = ups_pb2.UCommands()
                toDelivery.simspeed = simSpeed
                deliveryDetail = toDelivery.deliveries.add()
                deliveryDetail.truckid = loadedTruck
                # Add all the destinations to the truck's record
                for eachPack in amazonCmd.packages_loaded.package_id:
                    # Retrieve the destination of the package
                    getPackDes = {}
                    getPackDes["pack_id"] = eachPack
                    packDes = requests.post(aGetPackDes, json=getPackDes)
                    packDesJson = packDes.json()
                    if packDesJson["result"] == 0:
                        eachDelivery = deliveryDetail.packages.add()
                        eachDelivery.packageid = eachPack
                        eachDelivery.x = packDesJson["x"]
                        eachDelivery.y = packDesJson["y"]
                        print("Package ID:",eachPack, "x:", packDesJson["x"], "y", packDesJson["y"])
                    else:
                        print("Cannot get package ", eachPack, " destination")

                # Send the truck out for delivery
                print("This is the message sent to delivery")
                print(toDelivery)
                sendMsg(self.toWorldSocket, toDelivery)
                print("Send truck", loadedTruck, "to delivery")
            elif truckResJson["result"] == -1:
                print("Truck ", loadedTruck, " has some problem while set to be in delivery")
                
        else:
            print("Amazon sent an ill-formed Acommand")
            self.clientSocket.close()

# This is a function that will listen to the world's response
def listenToWorld(worldSocket):
    while True:
        worldResponse = ups_pb2.UResponses()
        worldResponseMsg = recvMsg(worldSocket)
        worldResponse.ParseFromString(worldResponseMsg)
        print("-----------------------")
        print("Message from the world")
        print(worldResponse)
        
        # Check the message content of the response from world
        if worldResponse.HasField("error"):
            print("The response from the world has an error!")
        elif len(worldResponse.completions) != 0:
            # The truck has been sent to the destinations
            for finished in worldResponse.completions:
                finished_truckID = finished.truckid
                finished_x = finished.x
                finished_y = finished.y
                # Connect to the database to confirm the next operation
                # If it arrived at the warehouse, send the info to Amazon
                # If it finished delivery, send it back to the next job
                  # It will search through the available task table
                  # If there is another task, send the truck back to the destination warehouse
                  # Else, just set it to be idle
                # If it is just finished after initialization, search through the task list as well
                # So it will be the same as after finishing in delivery. It will return arrived_at deelivery
                complete = {}
                complete["truck_id"] = finished_truckID
                complete["x"] = finished_x
                complete["y"] = finished_y
                responRaw = requests.post(wTruckCompletion, json=complete)
                respon = responRaw.json()
                if respon["arrived_at"] == "warehouse":
                    # Send Amazon truck has arrived at which warehouse
                    try:
                        sendAMZ = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                    except:
                        print("Cannot create the socket to connect to Amazon")

                    truckArrived = amazonUPS_pb2.Ucommands()
                    truckArrived.truck_arrival.truck_id = finished_truckID
                    truckArrived.truck_arrival.wh_id = respon["wh_id"]
                    sendAMZ.connect((amazonHost, amazonListenPort))

                    # Send the arrival message to Amazon
                    print("Send the truck arrival message to Amazon")
                    sendMsg(sendAMZ, truckArrived)
                    
                    # Wait for Amazon's response
                    amzResponse = amazonUPS_pb2.Aresponses()
                    amzMsg = recvMsg(sendAMZ)
                    amzResponse.ParseFromString(amzMsg)
                    sendAMZ.close() # Close the socket
                    print("**********************")
                    print("The Amazon Ack is")
                    print(amzResponse)
                    '''
                    if amzResponse..success == True:
                        # The confirmation is correct
                        print("The amazon response sent a success message")
                    else:
                        # Handle the error
                        print("The amazon response sent an error message")
                    '''
                            
                elif respon["arrived_at"] == "delivery":
                    # The truck has finished all of its delivery
                    # The task of searching for the next available task is in the backend
                    # Or here should be an field of nextWH
                    if respon["nextWH"] != -1:
                        # There is a next pickup
                        nextPickUp = ups_pb2.UCommands()
                        nextPickUp.simspeed = simSpeed
                        pickUpLocation = nextPickUp.pickups.add()
                        pickUpLocation.truckid = finished_truckID
                        pickUpLocation.whid = respon["nextWH"]
                        sendMsg(worldSocket, nextPickUp)
                else:
                    # There is something wrong here
                    print("Truck ", finished_truckID, " has some issues with the received completion message")
        elif len(worldResponse.delivered) != 0:
            # The truck has finished at least one delivery along the road
            for deliveredPack in worldResponse.delivered:
                delivered_truck = deliveredPack.truckid
                delivered_packageid = deliveredPack.packageid
                # Connect to the database to update the package information
                packInfo = {}
                packInfo["packageID"] = delivered_packageid
                deliveryRes = requests.post(wPackageDelivered, json=packInfo)
                deliveryResJson = deliveryRes.json()
                if deliveryResJson["result"] == 0:
                    print("Package ID ",  delivered_packageid, " has been delivered by truck ", delivered_truck)
                else:
                    print("There is something wrong with this delivered package", delivered_packageid)

                # Send a notification to Amazon
                amzPackArrived_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                amzPackArrived_sock.connect((amazonHost, amazonListenPort))
                
                arrivalNotification = amazonUPS_pb2.Ucommands()
                arrivalNotification.package_deliver.package_id = delivered_packageid
                sendMsg(amzPackArrived_sock, arrivalNotification)
                
                # Receive an ack from Amazon
                aPackAck = amazonUPS_pb2.Aresponses()
                aPackAckMsg = recvMsg(amzPackArrived_sock)
                aPackAck.ParseFromString(aPackAckMsg)
                amzPackArrived_sock.close()

                print("**********************")
                print("The ack from Amazon")
                print(aPackAck)

                '''
                if aPackAck.acknowledge.success == True:
                    print("Amazon has received the package delivered notification")
                else:
                    print("Amazon has failed to receive the package delivered notification")
                    print(aPackAck.acknowledge.error)
                '''
                    
        elif worldResponse.HasField("finished"):
            if (worldResponse.finished == True):
                print("The world has sent the message to disconnect")
                worldSocket.close()
        else:
            print("The world has sent an invalid UResponses")

# Establish Connection to the World Machine
try:
    worldSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
except:
    print("Cannot create the socket to connect to the world")
    sys.exit()

worldSocket.connect((worldHost, worldPort))
print("worldHost", worldHost)
print("worldPort", worldPort)

# Connect to the Specified World
worldConnection = ups_pb2.UConnect()
worldConnection.numtrucksinit = numTrucksInit
print(worldConnection)
sendMsg(worldSocket, worldConnection)

# Receive Connected Confirmation
worldConnected = ups_pb2.UConnected()
connectedMsg = recvMsg(worldSocket)
worldConnected.ParseFromString(connectedMsg)
print(worldConnected)

if worldConnected.HasField("error"):
    print("There is an error while connecting to the world. The error is")
    print(worldConnected.error)
else:
    print("The world ID is", worldConnected.worldid)

# Spawn the Listen to World Thread
listenToWorldThread = threading.Thread(target = listenToWorld, args=(worldSocket,))
listenToWorldThread.start()

# Set up the Socket for Connecting to Amazon
amazonSocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
try:
    amazonSocket.bind(("", amazonPort))
except socket.error as e:
    print(str(e))
    
amazonSocket.listen(10)

# This part can be used to spawn a thread with no returning value
# Instead of a blocking call
while True:
    clientSocket, address = amazonSocket.accept()
    amzThread = AmazonCmd(clientSocket, worldSocket)
    amzThread.start()
