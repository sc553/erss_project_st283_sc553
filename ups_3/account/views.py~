from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from django.views import generic
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login, authenticate
from django.contrib.auth.models import User

from .models import Truck, Package, Task, Product

import json

# Create your views here.
class PackageView(generic.ListView):
    template_name = 'account/index.html'
    context_object_name = 'packages_list'

    def get_queryset(self):
        return Package.objects.all()

# User Signup
def signup(request):
    if request.method == "POST":
        f = UserCreationForm(request.POST)
        if f.is_valid():
            f.save()
            user = authenticate(username=f.username, password=f.password1)
            login(request, user)
            redirect("index")
    else:
        f = UserCreationForm()

    return render(request, 'account/signup.html', {'form': f})

    
def tracking(request):
    return render(request, 'account/tracking.html')

def searchPackage(request):
    try:
        selected_package = Package.objects.get(pk=request.POST['tracking_id'])
    except (KeyError, Package.DoesNotExist):
        return render(request, 'account/tracking.html', {
            'error_message': "The package ID doesn't exist.",
        })
    else:
        return HttpResponseRedirect(reverse('account:index'))

@csrf_exempt
def aRequestTruck(request):
    if request.method == "POST":
        data = request.body.decode('utf-8')
        receivedData = json.loads(data)
        warehouse_id = receivedData["wh_id"]
        # Search Through all the trucks to see which one is available
        if Truck.objects.filter(status=0).exists():
            freeTruck = Truck.objects.filter(status=0)[0]
            freeTruck.status = 1
            freeTruck.warehouseID = warehouse_id
            freeTruck.save()
            print("The free truck is truckID", freeTruck.truck_id)
            # Change the stauts of all the pending packages at that warehouse to truck en route
            packages_set = Package.objects.filter(whID=warehouse_id, deliveryStatus=0)
            if packages_set.exists():
                for pack in packages_set:
                    print("Package ID ", pack.pk, " is at the warehouse", warehouse_id)
                    pack.deliveryStatus = 1
                    pack.save()
        
            return JsonResponse({'truck_id': freeTruck.truck_id})
        else:
            # There is no free truck. Put it on the Task Table
            newTask = Task(whID=warehouse_id)
            newTask.save()
            print("There is no free truck")
            return JsonResponse({'truck_id': -1})
    else:
        return HttpResponse("You are not authorized to view this page")
    
@csrf_exempt
def aRequestPackID(request):
    if request.method == "POST":
        data = request.body.decode('utf-8')
        receivedData = json.loads(data)
        user = receivedData["user"]
        #itemID = receivedData["itemID"]
        #itemDsp = receivedData["description"]
        #itemQuantity = receivedData["quantity"]    
        destination_x = receivedData["x"]
        destination_y = receivedData["y"]
        warehouse_id = receivedData["whID"]

        # Need to find out if the user is an input since it is optional
        if user == -1:
            # There is no user field
            newPackage = Package(destinationX=destination_x, destinationY=destination_y, whID=warehouse_id, deliveryStatus=0)
        else:
            # UserID is the User object, not just the user pk
            try:
                theUser = User.objects.get(pk=user)
            except User.DoesNotExist:
                # User doesn't exist, return an invalid packID
                return JsonResponse({"newID": -1})
            
            newPackage = Package(userID=theUser, destinationX=destination_x, destinationY=destination_y, whID=warehouse_id, deliveryStatus=0)
        
        newPackage.save()
        newPackID = newPackage.pk

        # Find out if there's a truck en route to that warehouse
        trucks_set = Truck.objects.filter(status=1, warehouseID=warehouse_id)
        if trucks_set.exists():
            # Change the package status to truck en route
            newPackage.deliveryStatus = 1
            newPackage.save()
        else:
            print("No truck is heading to this warehouse", warehouse_id)
        # Return the new packID
        return JsonResponse({"newID": newPackID})
    else:
        return HttpResponse("You are not authorized to view this page")


@csrf_exempt
def aAddProduct(request):
    if request.method == "POST":
        data = request.body.decode('utf-8')
        receivedData = json.loads(data)
        aPack_id = receivedData["pack_id"]
        aItem_id = receivedData["item_id"]
        aDescription = receivedData["description"]
        aQuantity = receivedData["amount"]
        # Find the associated package
        try:
            thePack = Package.objects.get(pk=aPack_id)
        except Package.DoesNotExist:
            print("The packgeid", aPack_id, "send to add a product does not exist")
            return JsonResponse({"status": -1})

        newProduct = Product(pack=thePack, item_id=aItem_id, description=aDescription, quantity=aQuantity)
        newProduct.save()
        return JsonResponse({"status": 0})
        
    else:
        return HttpResponse("You are not authorized to view this page")

@csrf_exempt
def aLoaded(request):
    if request.method == "POST":
        data = request.body.decode('utf-8')
        receivedData = json.loads(data)
        loadedPackID = receivedData["packID"]
        try:
            pack = Package.objects.get(pk=loadedPackID)
        except Package.DoesNotExist:
            print("The loaded packageid amazon provided doesn't exist")
            return JsonResponse({"status": -1})

        pack.deliveryStatus = 3
        pack.save()
        return JsonResponse({"status": 0})
    else:
        return HttpResponse("You are not authorized to view this page")

@csrf_exempt
def aTruckToGo(request):
    if request.method == "POST":
        data = request.body.decode('utf-8')
        receivedData = json.loads(data)
        truckID = receivedData["truckID"]
        whID = receivedData["whID"]
        try:
            theTruck = Truck.objects.get(truck_id=truckID)
        except Truck.DoesNotExist:
            print("The truck ID given doesn't exist: ", truckID)
            return JsonResponse({"result": -1})
        if theTruck.warehouseID != whID:
            print("The truck ID's warehouse is wrong")
            return JsonResponse({"result": -1})
        theTruck.status = 3
        theTruck.save()
        return JsonResponse({"result": 0})
    else:
        return HttpResponse("You are not authorized to view this page")
            
@csrf_exempt
def aGetPackDes(request):
    if request.method == "POST":
        data = request.body.decode('utf-8')
        receivedData = json.loads(data)
        pack_id = receivedData["pack_id"]
        try:
            thePack = Package.objects.get(pk=pack_id)
        except Package.DoesNotExist:
            print("The package loaded for truck doesn't exist")
            return JsonResponse({"result": -1})
        x = thePack.destinationX
        y = thePack.destinationY
        return JsonResponse({"result": 0, "x": x, "y": y})
    else:
        return HttpResponse("You are not authorized to view this page")

@csrf_exempt
def wPackageDelivered(request):
    if request.method == "POST":
        data = request.body.decode('utf-8')
        receivedData = json.loads(data)
        packArrived = receivedData["packageID"]
        try:
            pack = Package.objects.get(pk=packArrived)
        except Package.DoesNotExist:
            print("The package id sent by amazon doesn't exist ", packArrived)
            return JsonResponse({"result": -1})
        pack.deliveryStatus = 4
        pack.save()
        return JsonResponse({"result": 0})
    else:
        return HttpResponse("You are not authorized to view this page")

@csrf_exempt
def wTruckCompletion(request):
    if request.method == "POST":
        data = request.body.decode('utf-8')
        receivedData = json.loads(data)
        finished_truckID = receivedData["truck_id"]
        # Check where the truck has arrived at
        try:
            theTruck = Truck.objects.get(truck_id=finished_truckID)
        except Truck.DoesNotExist:
            # The truck doesn't exist
            # It means it just finished initialization, so save it to the model
            x = receivedData["x"]
            y = receivedData["y"]
            newTruck = Truck(truck_id=finished_truckID, posX=x, posY=y, warehouseID=-1, status=0)
            newTruck.save()
            print("New Truck has been saved to the database with truck ID", finished_truckID, "x", x, "y", y)
            # Search through the task table, and find out if there is any pending task
            if Task.objects.all().exists():
                nextPickUp = Task.objects.all()[0]
                wh_id = nextPickUp.whID
                nextPickUp.delete()
                newTruck.status = 1
                newTruck.save()
                print("New truck is sent to pick up")
                return JsonResponse({'arrived_at': "delivery", 'nextWH': wh_id})
            else:
                return JsonResponse({'arrived_at': "delivery", 'nextWH': -1})
            
        if theTruck.status == 1:
            # The truck arrived at the warehouse
            # Set the truck's status to at the warehouse
            theTruck.status = 2
            theTruck.save()
            
            # Change all the package waiting at that warehouse to be truck waiting
            packages_set = Package.objects.filter(deliveryStatus=1, whID=theTruck.warehouseID)
            if packages_set.exists():
                for pack in packages_set:
                    pack.deliveryStatus = 2
                    pack.save()
                    
            return JsonResponse({'arrived_at': "warehouse", 'wh_id': theTruck.warehouseID})
        elif theTruck.status == 3:
            # The truck has finished the delivery
            # Search through the task list to find out if there is any pending task
            print("Truck ID", theTruck.truck_id, "just finished delivery")
            if Task.objects.all().exists():
                nextPickUp = Task.objects.all()[0]
                wh_id = nextPickUp.whid
                nextPickUp.delete()
                theTruck.status = 1
                theTruck.save()
                return JsonResponse({'arrived_at': "delivery", 'nextWH': wh_id})
            else:
                theTruck.status = 0
                theTruck.save()
                return JsonResponse({'arrived_at': "delivery", 'nextWH': -1})
        else:
            print("This truck ", finished_truckID, " has the wrong status")
            return JsonResponse({'arrived_at': "error"})
    else:
        return HttpResponse("You are not authorized to view this page")
            
    
def world(request):
    return HttpResponse("Hello World")
