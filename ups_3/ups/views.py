from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.views import generic
from django.views.decorators.csrf import csrf_exempt
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth import login, logout, authenticate
from django.contrib.auth.models import User
from copy import deepcopy
from django import forms
from .forms import *
from .models import *

import json

# Create your views here.
class UserForm(forms.Form):
    username = forms.CharField(label = 'username',max_length=100)
    email = forms.CharField(label = 'email',max_length=50)
    password = forms.CharField(label = 'password',max_length=50,widget=forms.PasswordInput())

class PackageView(generic.ListView):
    template_name = 'ups/index.html'
    context_object_name = 'packages_list'

    def get_queryset(self):
        return Package.objects.all()
# User Signup
def signup(request):
    if request.method == "POST":
        f = registerForm(request.POST)
        if f.is_valid():
            f.save()
            username = f.cleaned_data.get('username')
            filterResult = User.objects.filter(username = username)
            existing=[]
            if len(filterResult)>0:
                return render(request,'ups/signup.html',{'username already been taken':existing})
            else:
                password1 = f.cleaned_data.get('password1')
                password2 = f.cleaned_data.get('password2')
                errors = []
                if (password2 != password1):
                    errors.append("Password is not repeated correctly!")
                    return render(request, 'ups/signup.html',{'errors':errors})
                password = password2
                email = f.cleaned_data.get('email')
                user = User.objects.create(username=username,password=password1)
                user.save()
                userid = user.pk
                return render(request,'ups/usertracking.html', {'userid': userid})                       
    else:
        f = registerForm()
    return render(request, 'ups/signup.html', {'f': f})

def signin(request):
    if request.method == 'POST':
        uf = UserForm(request.POST)
        if uf.is_valid():
            username = uf.cleaned_data['username']
            password = uf.cleaned_data['password']
        username = request.POST['username']
        raw_password = request.POST['password']
        user = authenticate(username = username, password = raw_password)
        if user is not None:
            if user.is_active:
                login(request,user)
                return redirect('ups/tracking/')
            else:
                return render(request, 'ups/login.html',{'uf':uf, 'incorrect user':errors})
        else:
            return render(request,'ups/login.html',{'uf':uf, 'incorrect password':errors})
    else:
        uf = UserForm()
    return render(request,'ups/login.html',{'uf':uf})

@login_required
def signout(request):
    logout(request)
    return render(request,'ups/tracking.html')

@login_required
def changeDes(request):
    #package = Package.objects.get(package_id = pk)
    if request.method == "POST":
        if "package_id" in request.POST:
            pack_id = request.POST["package_id"]
        else:
            return render(request, 'ups/changeDes.html', {'errorMsg': 'The input package ID is invalid'})
        if "new_destination_X" in request.POST:
            new_destination_X = request.POST["new_destination_X"]
        else:
            return render(request, 'ups/changeDes.html', {'errorMsg': 'The input destination X is invalid'})
        if "new_destination_Y" in request.POST:
            new_destination_Y =request.POST["new_destination_Y"]
        else:
            return render(request, 'ups/changeDes.html', {'errorMsg': 'The input destination Y is invalid'})

        # Check if the package exists
        try:
            thePack = Package.objects.get(pk=pack_id)
        except Package.DoesNotExist:
            return render(request, 'ups/changeDes.html', {'errorMsg': 'The package does not exist'})

        # Check the user identity
        print("True Pack", thePack.userID.pk)
        print("I'm", request.user.id)
        if thePack.userID is None:
            return render(request, 'ups/changeDes.html', {'errorMsg': 'You are not authoized to change this package'})
        else:
            trueUserID = thePack.userID.pk
            currUser = request.user.id
            if trueUserID != trueUserID:
                return render(request, 'ups/changeDes.html', {'errorMsg': 'You are not authoized to change this package'})
        
        # Check the status of the package
        status = thePack.deliveryStatus
        if status !=3 and status != 4:
            thePack.destinationX = new_destination_X
            thePack.destinationY = new_destination_Y
            thePack.save()
            return render(request, 'ups/changeDes.html', {'errorMsg': 'Success!'})
        
        else:
            return render(request, 'ups/changeDes.html', {'errorMsg': 'The package is out for delivery'})
    else:
        return render(request, 'ups/changeDes.html')

def packStatus(deliveryStatus):
    if deliveryStatus == 0:
        return 'Created'
    elif deliveryStatus == 1:
        return 'Truck en route to warehouse'
    elif deliveryStatus == 2:
        return 'Waiting to be loaded at warehouse'
    elif deliveryStatus == 3:
        return 'Out for delivery'
    else:
        return 'Delivered'
    
@login_required
def package_detail(request):
    currUser = request.user
    allPacks = Package.objects.filter(userID=currUser)
    if len(allPacks) == 0:
        # There is no package associated with this user
        return render(request, 'ups/package_detail.html')
    else:
        # There is at least one package associated with the current user
        userPackages = []
        for pack in allPacks:
            currPack = deepcopy(pack)
            cond = packStatus(pack.deliveryStatus)
            currPack.condition = cond
            currPack.product = []
            prodInPack = Product.objects.filter(pack=pack)
            for prod in prodInPack:
                print(prod.description)
                theProd = deepcopy(prod)
                currPack.product.append(theProd)

            userPackages.append(currPack)
            
        return render(request, 'ups/package_detail.html', {'allPacks': userPackages})


def tracking(request):
    return render(request, 'ups/tracking.html')

@login_required
def usertracking(request):
    userid = request.user.id
    return render(request, 'ups/usertracking.html',{'userid': userid})

def searchPackage(request):
    if request.method == "POST":
        track_id = request.POST.get("tracking_id", -1)
        print("the tracking id is", track_id)
        try:
            selected_package = Package.objects.get(pk=request.POST['tracking_id'])
        except (KeyError, Package.DoesNotExist):
            return render(request, 'ups/tracking.html', {
                'error_message': "The package ID doesn't exist.",
            })
        
        return render(request, 'ups/tracking.html', {'pack_id': selected_package.pk,
                                                     'pack_status': packStatus(selected_package.deliveryStatus),})
        #else:
            #return HttpResponseRedirect(reverse('ups:index'))
    else:
        return render(request, 'ups/tracking.html')
        
    

@csrf_exempt
def aRequestTruck(request):
    if request.method == "POST":
        data = request.body.decode('utf-8')
        receivedData = json.loads(data)
        warehouse_id = receivedData["wh_id"]
        # Search Through all the trucks to see which one is available
        if Truck.objects.filter(status=0).exists():
            freeTruck = Truck.objects.filter(status=0)[0]
            freeTruck.status = 1
            freeTruck.warehouseID = warehouse_id
            freeTruck.save()
            print("The free truck is truckID", freeTruck.truck_id)
            # Change the stauts of all the pending packages at that warehouse to truck en route
            packages_set = Package.objects.filter(whID=warehouse_id, deliveryStatus=0)
            if packages_set.exists():
                for pack in packages_set:
                    print("Package ID ", pack.pk, " is at the warehouse", warehouse_id)
                    pack.deliveryStatus = 1
                    pack.save()
        
            return JsonResponse({'truck_id': freeTruck.truck_id})
        else:
            # There is no free truck. Put it on the Task Table
            newTask = Task(whID=warehouse_id)
            newTask.save()
            print("There is no free truck")
            return JsonResponse({'truck_id': -1})
    else:
        return HttpResponse("You are not authorized to view this page")
    
@csrf_exempt
def aRequestPackID(request):
    if request.method == "POST":
        data = request.body.decode('utf-8')
        receivedData = json.loads(data)
        user = receivedData["user"]
        #itemID = receivedData["itemID"]
        #itemDsp = receivedData["description"]
        #itemQuantity = receivedData["quantity"]    
        destination_x = receivedData["x"]
        destination_y = receivedData["y"]
        warehouse_id = receivedData["whID"]

        # Need to find out if the user is an input since it is optional
        if user == -1:
            # There is no user field
            newPackage = Package(destinationX=destination_x, destinationY=destination_y, whID=warehouse_id, deliveryStatus=0)
        else:
            # UserID is the User object, not just the user pk
            try:
                theUser = User.objects.get(pk=user)
            except User.DoesNotExist:
                # User doesn't exist, return an invalid packID
                return JsonResponse({"newID": -1})
            
            newPackage = Package(userID=theUser, destinationX=destination_x, destinationY=destination_y, whID=warehouse_id, deliveryStatus=0)
        
        newPackage.save()
        newPackID = newPackage.pk
        print("New Package ID isssues", newPackID)

        # Find out if there's a truck en route to that warehouse
        trucks_set = Truck.objects.filter(status=1, warehouseID=warehouse_id)
        if trucks_set.exists():
            # Change the package status to truck en route
            newPackage.deliveryStatus = 1
            newPackage.save()
        else:
            print("No truck is heading to this warehouse", warehouse_id)
        # Return the new packID
        return JsonResponse({"newID": newPackID})
    else:
        return HttpResponse("You are not authorized to view this page")


@csrf_exempt
def aAddProduct(request):
    if request.method == "POST":
        data = request.body.decode('utf-8')
        receivedData = json.loads(data)
        aPack_id = receivedData["pack_id"]
        aItem_id = receivedData["item_id"]
        aDescription = receivedData["description"]
        aQuantity = receivedData["amount"]
        # Find the associated package
        try:
            thePack = Package.objects.get(pk=aPack_id)
        except Package.DoesNotExist:
            print("The packgeid", aPack_id, "send to add a product does not exist")
            return JsonResponse({"status": -1})

        newProduct = Product(pack=thePack, item_id=aItem_id, description=aDescription, quantity=aQuantity)
        newProduct.save()
        print("New product is added")
        print(aItem_id, aDescription, aQuantity)
        return JsonResponse({"status": 0})
        
    else:
        return HttpResponse("You are not authorized to view this page")

@csrf_exempt
def aLoaded(request):
    if request.method == "POST":
        data = request.body.decode('utf-8')
        receivedData = json.loads(data)
        loadedPackID = receivedData["packID"]
        try:
            pack = Package.objects.get(pk=loadedPackID)
        except Package.DoesNotExist:
            print("The loaded packageid amazon provided doesn't exist")
            return JsonResponse({"status": -1})

        pack.deliveryStatus = 3
        pack.save()
        return JsonResponse({"status": 0})
    else:
        return HttpResponse("You are not authorized to view this page")

@csrf_exempt
def aTruckToGo(request):
    if request.method == "POST":
        data = request.body.decode('utf-8')
        receivedData = json.loads(data)
        truckID = receivedData["truckID"]
        whID = receivedData["whID"]
        try:
            theTruck = Truck.objects.get(truck_id=truckID)
        except Truck.DoesNotExist:
            print("The truck ID given doesn't exist: ", truckID)
            return JsonResponse({"result": -1})
        if theTruck.warehouseID != whID:
            print("The truck ID's warehouse is wrong")
            return JsonResponse({"result": -1})
        theTruck.status = 3
        theTruck.save()
        return JsonResponse({"result": 0})
    else:
        return HttpResponse("You are not authorized to view this page")
            
@csrf_exempt
def aGetPackDes(request):
    if request.method == "POST":
        data = request.body.decode('utf-8')
        receivedData = json.loads(data)
        pack_id = receivedData["pack_id"]
        try:
            thePack = Package.objects.get(pk=pack_id)
        except Package.DoesNotExist:
            print("The package loaded for truck doesn't exist")
            return JsonResponse({"result": -1})
        x = thePack.destinationX
        y = thePack.destinationY
        return JsonResponse({"result": 0, "x": x, "y": y})
    else:
        return HttpResponse("You are not authorized to view this page")

@csrf_exempt
def wPackageDelivered(request):
    if request.method == "POST":
        data = request.body.decode('utf-8')
        receivedData = json.loads(data)
        packArrived = receivedData["packageID"]
        try:
            pack = Package.objects.get(pk=packArrived)
        except Package.DoesNotExist:
            print("The package id sent by amazon doesn't exist ", packArrived)
            return JsonResponse({"result": -1})
        pack.deliveryStatus = 4
        pack.save()
        return JsonResponse({"result": 0})
    else:
        return HttpResponse("You are not authorized to view this page")

@csrf_exempt
def wTruckCompletion(request):
    if request.method == "POST":
        data = request.body.decode('utf-8')
        receivedData = json.loads(data)
        finished_truckID = receivedData["truck_id"]
        # Check where the truck has arrived at
        try:
            theTruck = Truck.objects.get(truck_id=finished_truckID)
        except Truck.DoesNotExist:
            # The truck doesn't exist
            # It means it just finished initialization, so save it to the model
            x = receivedData["x"]
            y = receivedData["y"]
            newTruck = Truck(truck_id=finished_truckID, posX=x, posY=y, warehouseID=-1, status=0)
            newTruck.save()
            print("New Truck has been saved to the database with truck ID", finished_truckID, "x", x, "y", y)
            # Search through the task table, and find out if there is any pending task
            if Task.objects.all().exists():
                nextPickUp = Task.objects.all()[0]
                wh_id = nextPickUp.whID
                nextPickUp.delete()
                newTruck.status = 1
                newTruck.save()
                print("New truck is sent to pick up")
                return JsonResponse({'arrived_at': "delivery", 'nextWH': wh_id})
            else:
                return JsonResponse({'arrived_at': "delivery", 'nextWH': -1})
            
        if theTruck.status == 1:
            # The truck arrived at the warehouse
            # Set the truck's status to at the warehouse
            theTruck.status = 2
            theTruck.save()
            
            # Change all the package waiting at that warehouse to be truck waiting
            packages_set = Package.objects.filter(deliveryStatus=1, whID=theTruck.warehouseID)
            if packages_set.exists():
                for pack in packages_set:
                    pack.deliveryStatus = 2
                    pack.save()
                    
            return JsonResponse({'arrived_at': "warehouse", 'wh_id': theTruck.warehouseID})
        elif theTruck.status == 3:
            # The truck has finished the delivery
            # Search through the task list to find out if there is any pending task
            print("Truck ID", theTruck.truck_id, "just finished delivery")
            if Task.objects.all().exists():
                nextPickUp = Task.objects.all()[0]
                wh_id = nextPickUp.whid
                nextPickUp.delete()
                theTruck.status = 1
                theTruck.save()
                return JsonResponse({'arrived_at': "delivery", 'nextWH': wh_id})
            else:
                theTruck.status = 0
                theTruck.save()
                return JsonResponse({'arrived_at': "delivery", 'nextWH': -1})
        else:
            print("This truck ", finished_truckID, " has the wrong status")
            return JsonResponse({'arrived_at': "error"})
    else:
        return HttpResponse("You are not authorized to view this page")
            
    
def world(request):
    return HttpResponse("Hello World")
