from django import forms
from django.forms import ModelForm,modelformset_factory
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .models import *

class registerForm(UserCreationForm):
    username   = forms.CharField(max_length=100)
    password1  = forms.CharField(widget=forms.PasswordInput())
    password2  = forms.CharField(widget=forms.PasswordInput())
    email = forms.EmailField()
    class Meta:
        model = User
        fields = ('username','password1', 'password2','email')

        def __init__(self,*args, **kwargs):
            super(registerForm, self).__init__(*args, **kwargs)
            
