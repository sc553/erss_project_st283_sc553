from django.db import models
from django.contrib.auth.models import User

# Create your models here.

# All the info about the truck
class Truck(models.Model):
    truck_id = models.IntegerField(default=-1)
    posX = models.IntegerField(default=0)
    posY = models.IntegerField(default=0)
    warehouseID = models.IntegerField(default=-1)
    status = models.IntegerField(default=0)
    # 0=idle, 1=to_warehouse, 2=at_warehouse 3=delivery

# All the info about a package
class Package(models.Model):
    userID = models.ForeignKey(User, on_delete=models.CASCADE, blank=True, null=True)
    #details = models.CharField(max_length=200) # C
    destinationX = models.IntegerField(default=0)
    destinationY = models.IntegerField(default=0)
    whID = models.IntegerField(default=0)
    #quantity = models.IntegerField(default=0) # C
    deliveryStatus = models.IntegerField(default=0)
    # 0=created, 1=truck en route, 2=truck waiting, 3=out for delivery, 4=delivered

# All the info about a waiting warehouse call from Amazon
class Task(models.Model):
    whID = models.IntegerField(default=0)


# All the Details of a single item
class Product(models.Model):
    pack = models.ForeignKey(Package, on_delete=models.CASCADE)
    item_id = models.IntegerField(default=-1)
    description = models.CharField(max_length=500)
    quantity = models.IntegerField(default=0)
