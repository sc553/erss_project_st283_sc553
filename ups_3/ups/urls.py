from django.urls import path
from django.conf.urls import url

from . import views

app_name = 'ups'
urlpatterns = [
    path('', views.PackageView.as_view(), name='index'),
    #path(r'signup/', views.signup, name='signup'),
    #path(r'tracking/', views.tracking, name='tracking'),
    path(r'searchPackage/', views.searchPackage, name='searchPackage'),
    path(r'world/', views.world, name='world'),
    #path(r'result/', views.result, name='result'),
    # The following are the web page that should be used internally
    # for communicating between the daemon and the database
    path(r'aRequestTruck/', views.aRequestTruck, name='aRequestTruck'),
    path(r'aRequestPackID/', views.aRequestPackID, name='aRequestPackID'),
    path(r'aAddProduct/', views.aAddProduct, name='aAddProduct'),
    path(r'aLoaded/', views.aLoaded, name='aLoaded'),
    path(r'aTruckToGo/', views.aTruckToGo, name='aTruckToGo'),
    path(r'aGetPackDes/', views.aGetPackDes, name='aGetPackDes'),
    path(r'wTruckCompletion/', views.wTruckCompletion, name='wTruckCompletion'),
    path(r'wPackageDelivered/', views.wPackageDelivered, name='wPackageDelivered'),

    url(r'^tracking/', views.tracking, name='tracking'),
    url(r'^usertracking/', views.usertracking, name='usertracking'),
    url(r'^signup/', views.signup, name='signup'),
    url(r'^login/', views.signin, name='signin'),
    url(r'^logout/', views.signout, name='signout'),
    url(r'^changeDes/', views.changeDes, name='changeDes'),
    url(r'^package_detail/', views.package_detail, name='package_detail'),
]
